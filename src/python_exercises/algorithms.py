"""
These are function related to the bioinformaticts questions.
"""

from typing import List, Tuple, Iterable
#from bioinfo_exercises_solutions import solutions


def buildCrossingGraph(dow: Iterable) -> List[Tuple[str, str]]:
  """
  Build the intersection graph given a double occurence word.

  @param dow: a double occurrence word
  @return:    the edges of the intersection graph
  """
  pass


def getLongestSubstring(s1: str, s2: str) -> str:
  """
  Return the longest common substring between the two strings.
  """
  pass


def mst(matrix: List[List[float]], drawgraph=False) -> List[Tuple[int, int]]:
  """
  Given a 2D list of numbers, return a minimum spanning tree on the implied
  graph.  The first dimension is vertex 0, etc.

  @param matrix:    matrix specifying edge weights. weight 0 in non-edge
  @type matrix:     2D list
  @param drawgraph: display the graph if True
  @type drawgraph:  boolean

  @return: the list of edges in the minimum spanning tree
  """
  pass