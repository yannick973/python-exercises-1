import unittest

import random
import string

from python_exercises import algorithms
try:
  from python_exercises_solutions import solutions
except:
  print(f'Solutions package not present!! Random test will not run.')

import networkx as nx
import matplotlib.pyplot as plt




################################################################################
class TestLCS(unittest.TestCase):
  """
  Test algorithms for longest common substring.
  """

  def test_cases(self):
    s1, s2 = self.getInputFromFile('test/input/longestcommonsubstring_input0.txt')
    testsol = algorithms.getLongestSubstring(s1, s2)
    solution = self.getSolutionFromFile('test/solutions/longestcommonsubstring_sol0.txt')
    self.assertEqual(len(testsol), len(solution))
    self.assertTrue(testsol in solution)

    s1, s2 = self.getInputFromFile('test/input/longestcommonsubstring_input1.txt')
    testsol = algorithms.getLongestSubstring(s1, s2)
    solution = self.getSolutionFromFile('test/solutions/longestcommonsubstring_sol1.txt')
    self.assertEqual(len(testsol), len(solution))
    self.assertTrue(testsol in solution)


  def test_randomMutation(self):
    stringlen = 100
    mutationratio = .5

    alphabetlength = 15
    alphabet = string.ascii_lowercase[:alphabetlength]

    repetitions = 1000
    for _ in range(repetitions):
      self.compareRandom(stringlen, mutationratio, alphabet)


  def compareRandom(self, stringlen, mutationratio, alphabet):
    """
    Compare the two methods on a random generated string.
    """
    l1 = [random.choice(alphabet) for i in range(stringlen)]
    l2 = list(l1) 
    for _ in range(int(mutationratio*stringlen)):
      l2[random.randint(0, stringlen-1)] = random.choice(alphabet)

    s1 = ''.join(l1)
    s2 = ''.join(l2)
    self.compareSubstringMethods(s1, s2)


  def compareSubstringMethods(self, s1, s2):
    """
    Compare two methods on the given strings.
    """
    answer = solutions.longest_common_substring(s1, s2)
    testsol = algorithms.getLongestSubstring(s1, s2)

    self.assertEqual(len(answer), len(solutions.getLongestSubstring(s1, s2)))
    self.assertEqual(len(answer), len(testsol))
    self.assertTrue(testsol in s1 and testsol in s2)


  def getInputFromFile(self, infile):
    """
    Read in the strings from a file.
    """
    with open(infile) as f:
      strings = [line.rstrip() for line in f]
      self.assertEqual(len(strings), 2,
                      f'ERROR: {infile} does not have two lines.')
    return strings


  def getSolutionFromFile(self, solfile):
    with open(solfile) as f:
      return f.readline().strip()


################################################################################
class TestMST(unittest.TestCase):
  """
  Test algorithms for minimum spanning tree.
  """

  def test_randomGraphs(self):
    """
    Test on an erdos-renyi graph.
    """
    size = 10
    p = .5
    reps = 1000
    for _ in range(reps):
      self.compareRandomGraph(size, p)


  def compareRandomGraph(self, size, p):
    rg = nx.gnp_random_graph(size, p) 
    while(not nx.is_connected(rg)):       #Make sure the graph is connected.
      rg = nx.gnp_random_graph(size, p) 
    self.weightGraph(rg)

    edgesnx = list(nx.minimum_spanning_edges(rg))
    scorenx = sum(t[2]['weight'] for t in edgesnx)
    self.assertTrue(scorenx == self.scoreEdges(rg, ((i,j) for i,j,_ in edgesnx)))

    edgestestsol = algorithms.mst(self.getMatrix(rg))
    scoretestsol = self.scoreEdges(rg, edgestestsol)

    if(abs(scorenx - scoretestsol) > 0.0000000001):
      print('\nscores (solution, yourcode):', (scorenx, scoretestsol))
      print(edgestestsol)

      #Draw the graph:
      pos = nx.spring_layout(rg)
      nx.draw(rg, pos)
      edge_labels = {(u, v,): d['weight'] for u, v, d in rg.edges(data=True)}
      nx.draw_networkx_edge_labels(rg, pos, edge_labels=edge_labels)
      plt.show()

    self.assertEqual(len(edgesnx), len(edgestestsol), size-1)
    self.assertTrue(abs(scorenx - scoretestsol) < 0.0000000001)


  def scoreEdges(self, g, edges):
    """
    Sum the score of the edges from the given graph.
    """
    total = 0
    if(edges):
      for i,j in edges:
        self.assertTrue(i in g and j in g[i] and 'weight' in g[i][j])
        total += g[i][j]['weight']

    return total


  def weightGraph(self, G):
    """
    Assign random weights to edges.
    """
    for u, v in G.edges():
      G[u][v]['weight'] = random.random()


  def getMatrix(self, G):
    """
    Return a 2D matrix describing the given weighted graph.
    """
    size = len(G)
    matrix = []
    for _ in range(size):
      matrix.append([0]*size)

    for u, v in G.edges():
      matrix[u][v] = matrix[v][u] = G[u][v]['weight'] 
    return matrix


################################################################################
class TestCrossingGraph(unittest.TestCase):
  """
  Test algorithms for the construction of the crossing graph.
  """

  def test_randomDOWs(self):
    reps = 100
    n = 300
    for _ in range(reps):
      self.compareConstruction(n)


  def compareConstruction(self, n):
    dow = self.getDOW(n)
    edges1 = solutions.buildCrossingGraph(dow)
    edges2 = algorithms.buildCrossingGraph(dow)

    edges1 = sorted(sorted(e) for e in edges1)
    edges2 = sorted(sorted(e) for e in edges2)

    self.assertEqual(edges1, edges2)


  def getDOW(self, n):
    """
    Return a double occurrence word with n unique characters.
    """
    dow = list(range(n))*2
    random.shuffle(dow)
    return dow


if __name__ == '__main__':
  unittest.main()
